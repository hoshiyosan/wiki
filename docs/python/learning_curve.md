# Most important features

| Feature                | Description           | Validated? |
| ---------------------- | --------------------- | ---------- |
| decorators             | @myfunc               | v          |
| packing / unpacking    | * and ** operators    | v          |
| iterators / generators | range, next, __iter__ | x          |
| coroutines             | async, await          | x          |
| metaclasses            |                       | x          |
|                        |                       |            |
|                        |                       |            |
|                        |                       |            |

# Others utils features

| Feature          | Description       | Validated |
| ---------------- | ----------------- | --------- |
| collections      |                   | x         |
| context managers | with ctx_manager: | x         |