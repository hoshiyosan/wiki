# Gunicorn WSGI server

> A WSGI server is ...



## Create a virtualenv

It is better to run your application in an isolated environment to avoid side-effects and conflicts between differents modules / binaries versions.

Let's begin with installing the virtualenv module

```bash
pip install virtualenv
```

Once done, you can create your virtualenv by doing

```bash
virtualenv env
```

Where ```env``` is the name of the newly created virtualenv. There are many parameters available, for example to manually pick the python version to use. More info at https://virtualenv.pypa.io/en/latest/.

For the following I assume you have created your virtualenv like me (with the name env), and that you are working under your virtualenv. Do it by executing :

```bash
source env/bin/activate
```

## Create a basic app

Here we will create a simple example app using the micro-framework Flask. First install the framework this way

```bash
pip install Flask
```

And create the following file

*run.py*

```python
#!/usr/bin/python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "hello"

application = app 
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='1234')
```

You can test the app by running the following command

```bash
python run.py
```

Your app should be available at http://localhost:1234

## Gunicorn basics

Firstly, install gunicorn

```python
pip install gunicorn
```

You can then try to run your app with gunicorn to check that it is working with its WSGI server by doing :

```bash
env/bin/gunicorn --bind=127.0.0.1:1234 run:app
```

This time our app doesn't use Flask development server anymore, but Gunicorn's WSGI server. However, it should behaves exactly the same way.

In order to manage multiple app hosted with gunicorn, we could use a service called supervisor.

```bash
sudo apt-get install supervisor
```

WORKER CONFIGURATION

You can manage your workers like daemons

```bash
sudo supervisorctl start <worker_name>
sudo supervisorctl stop <worker_name>
sudo supervisorctl restart <worker_name>
```





https://openclassrooms.com/fr/courses/4425101-deployez-une-application-django/4688628-configurez-gunicorn-et-supervisor