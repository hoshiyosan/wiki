# Cookies

```js
function setCookie(sName, sValue) {
        var today = new Date(), expires = new Date();
        expires.setTime(today.getTime() + (365*24*60*60*1000));
        document.cookie = sName + "=" + JSON.stringify(sValue) + ";expires=" + expires.toGMTString();
}
```



```js
function getCookie(sName) {
        var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");
 
        if (oRegex.test(document.cookie)) {
                return JSON.parse(RegExp["$1"]);
        } else {
                return null;
        }
}
```



```js
function delCookie(sName) {
        var expires = new Date(); // expires now
        document.cookie = sName + "=;expires=" + expires.toGMTString();
}
```

