# Arrows handlers

```js
// fonction qui permet de scroll lors de l'appui sur les flèches
let height = 0;
function onArrow(evt){
	var sign = 1*(evt.key=="ArrowDown") - 1*(evt.key=="ArrowUp")
    height += sign * 20;
	window.scroll(0, height);
}
```

