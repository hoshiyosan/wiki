# Create your own aliases

An alias a way to link a keyword to any script you want, such as it will act like a binary program.

## How to create an alias

Each alias declaration must begin with the keyword ```alias``` and is followed by the name of the command to create. Then you just have to supply a single line bash command.

```bash
alias get-hour="date +%H:%M"
```

>  **Important:** Since aliases aren't persistent, you should probably save them somewhere so they can be loaded when starting a bash shell. The simplest way of doing this is by copying them to your ```~/.bashrc``` file. 
>
> However, to improve maintainability  

## Create aliases for all users



