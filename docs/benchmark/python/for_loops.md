# for loops comparisons

## To build simple lists

### Using a normal for

```python
def for1():
	t0 = time.time()
	l = []
	for k in range(10000):
	        l.append(k)
	t1 = time.time()
	print(t1-t0)
```

> 0.0039014816284179688 seconds

### Using an inline-for

```python
def for2():
     t0 = time.time()
     l = [ k for k in range(10000) ]
     t1 = time.time()
     print(t1-t0)
```

> 0.0020227432250976562

The 2d method is almost twice as fast as the 1st.

## To build lists with condition

### Using a normal for

```python
def for1():
	t0 = time.time()
	l = []
	for k in range(10000):
        if k%2 == 0:
	        l.append(k)
	t1 = time.time()
	print(t1-t0)
```

> 0.0008432865142822266

### Using an inline-for

```python
def for2():
     t0 = time.time()
     l = [ k for k in range(10000) if k%2 == 0 ]
     t1 = time.time()
     print(t1-t0)
```

> 0.0005881786346435547

